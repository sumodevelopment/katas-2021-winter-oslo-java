import { isAcquaintance, isFriend, noStranger, removeSpecialCharacters } from '.'

describe('removeSpecialCharacters', () => {

    test('This! is NOT, @Sparta! => This is NOT Sparta', () => {
        const source = 'This! is NOT, @Sparta!'
        const result = 'This is NOT Sparta'
        expect( removeSpecialCharacters(source) ).toBe( result )
    })

    test('!#$*($&(*#&$#Hi => Hi', () => {
        const source = '!#$*($&(*#&$#Hi'
        const result = 'Hi'
        expect( removeSpecialCharacters(source) ).toBe( result )
    })

})

describe('isAcquaintance', () => {
    test('{ hello: 3 } => true', () => {
        const source = { hello: 3 }
        expect( isAcquaintance(source, 'hello') ).toBe( true )
    })

    test('{ goodbye: 2 } => false', () => {
        const source = { goodbye: 2 }
        expect( isAcquaintance(source, 'goodbye') ).toBe( false )
    })
})

describe('isFriend', () => {
    test('{ friends: 5 } => true', () => {
        const source = { friends: 5 }
        expect( isFriend(source, 'friends') ).toBe( true )
    })

    test('{ enemy: 4 } => false', () => {
        const source = { enemy: 4 }
        expect( isFriend(source, 'enemy') ).toBe( false )
    })
})

describe('noStranger()', () => {
    test('See Spot run. See Spot jump. Spot likes jumping. See Spot fly. => [["spot", "see"], []]', () => {
        const source = 'See Spot run. See Spot jump. Spot likes jumping. See Spot fly.'
        const result = [["spot", "see"], []]
        expect( noStranger( source ) ).toEqual( result )
    })

    test("Well, isn't this just fun. Deep in a well, this is fun. Fun is fun. => [['well'], ['fun']]", () => {
        const source = 'Fun! Well, isn\'t this just fun. Deep in a well, this is fun. Well, Fun is fun.'
        const result = [["well"], ["fun"]]
        expect( noStranger( source ) ).toEqual( result )
    })

    test("Can't can't can't repeat some words. => [['can\'t'], []]", () => {
        const source = 'Can\'t can\'t can\'t repeat some words.'
        const result = [["can't"], []]
        expect( noStranger( source ) ).toEqual( result )
    })

    test("Hi, hi, hi. One word, one word, one word, one word, one word!!!!. => [['hi'], ['one','word']]", () => {
        const source = 'Hi, hi, hi. One word, one word, one word, one word, one word'
        const result = [['hi'], ['one', 'word']]
        expect( noStranger( source ) ).toEqual( result )
    })

    test("No repeats. => [[], []]", () => {
        const source = 'No repeats'
        const result = [[], []]
        expect( noStranger( source ) ).toEqual( result )
    })
})

