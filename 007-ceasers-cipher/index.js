export const ASCII_UPPERCASE_UPPER_THRESHOLD = 90
export const ASCII_UPPERCASE_LOWEST_THRESHOLD = 65
export const ASCII_LOWERCASE_UPPER_THRESHOLD = 122

// VERSION 1
export function isUpperCase(char) {
    const code = char.charCodeAt(0);
    return code <= ASCII_UPPERCASE_UPPER_THRESHOLD &&
        code >= ASCII_UPPERCASE_LOWEST_THRESHOLD;
}

export function isNotAlpha(char) {
    return char.charCodeAt(0) > ASCII_LOWERCASE_UPPER_THRESHOLD ||
        char.charCodeAt(0) < ASCII_UPPERCASE_LOWEST_THRESHOLD
}

export function caesarCipherV1(s, k) {

    return s.split('').map(char => {

        if (isNotAlpha(char)) {
            return char
        }

        const isUpper = isUpperCase(char)

        const lc = char.toLowerCase();
        let lcCode = lc.charCodeAt(0) + (k % ALPHABET_LENGTH)

        while (lcCode > 122) {
            lcCode -= ALPHABET_LENGTH;
        }

        let newChar = String.fromCharCode(lcCode)

        if (isUpper) {
            newChar = newChar.toUpperCase()
        }

        return newChar

    }).join('');
}


// VERSION 2
const ASCII_UPPER_START = 65
const ASCII_LOWER_START = 97
const ALPHABET_LENGTH = 26

export function startWithCode(char) {
    return char < "a" ? ASCII_UPPER_START : ASCII_LOWER_START
}

export function newCode(char, start, key) {
    return (char.charCodeAt(0) - start + key) % ALPHABET_LENGTH + start
}

export function caesarCipherV2(string, key) {
    return string.replace(/[a-z]/gi, char => {
        const start = startWithCode(char)
        return String.fromCharCode(newCode(char, start, key))
    })
}

// Source from Edabit 😱
export function caesarCipher(s, key) {
    const ALPHABET_LENGTH = 26
    return s.replace(/[a-z]/gi, currentChar => {
        const charCode = currentChar < "a" ? 65 : 97;
        return String.fromCharCode(
            (currentChar.charCodeAt(0) - charCode + key) % ALPHABET_LENGTH + charCode
        )
    });
}
