import { caesarCipher, caesarCipherV1, caesarCipherV2, isNotAlpha, isUpperCase } from './'

test('Ceasers Cipher Shortest', ()=> {
    const source = "middle-Outz"
    const output = "okffng-Qwvb"
    expect( caesarCipher(source, 2) ).toBe( output )
})

describe("Is special character", () => {
    test('! >> true', ()=> {
        expect(  isNotAlpha("!") ).toBe( true )
    })
    test('@ >> true', ()=> {
        expect(  isNotAlpha("@") ).toBe( true )
    })
    test('- >> true', ()=> {
        expect(  isNotAlpha("-") ).toBe( true )
    })
    test('a >> false', ()=> {
        expect(  isNotAlpha("a") ).toBe( false )
    })
    test('z >> false', ()=> {
        expect(  isNotAlpha("z") ).toBe( false )
    })
    test('2 >> true', ()=> {
        expect(  isNotAlpha("2") ).toBe( true )
    })
})

describe('Is uppercase character', () => {
    test('A >> true', ()=> {
        expect(  isUpperCase("A") ).toBe( true )
    })
    test('a >> false', ()=> {
        expect(  isUpperCase("a") ).toBe( false )
    })
    test('Z >> true', ()=> {
        expect(  isUpperCase("Z") ).toBe( true )
    })
    test('! >> false', ()=> {
        expect(  isUpperCase("!") ).toBe( false )
    })
})

describe("Ceasers Cipher", () => {
    test('middle-Outz >> okffng-Qwvb', ()=> {
        const source = "middle-Outz"
        const output = "okffng-Qwvb"
        expect( caesarCipherV2(source, 2) ).toBe( output )
    })

    test('Always-Look-on-the-Bright-Side-of-Life >> Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj', ()=> {
        const source = "Always-Look-on-the-Bright-Side-of-Life"
        const output = "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"
        expect( caesarCipherV2(source, 5) ).toBe( output )
    })

    test('A friend in need is a friend indeed >> U zlcyhx ch hyyx cm u zlcyhx chxyyx', ()=> {
        const source = "A friend in need is a friend indeed"
        const output = "U zlcyhx ch hyyx cm u zlcyhx chxyyx"
        expect( caesarCipherV2(source, 20) ).toBe( output )
    })
})
