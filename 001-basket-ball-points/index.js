// SOURCE CODE!
export function points(twoPointers, threePointers) {
    return calcTwoPointers(twoPointers) + calcThreePointers(threePointers)
}

export function calcTwoPointers(twoPointers) {
    return twoPointers * 2
}

export function calcThreePointers(threePointers) {
    return threePointers * 3
}