import { points, calcTwoPointers } from './index'

describe('calcTwoPointers', function () {
    test('calcTwoPointers(1) -> 2 ', function () {
        const source = 1
        const expected = 2
        expect( calcTwoPointers(source) ).toBe( expected )
    })
})

describe('Points', function () {
    test('points(1, 1) ➞ 5', function () {
        const expected = 5
        const twoP = 1
        const threeP = 1
        expect( points(twoP, threeP) ).toBe( expected )
    })
    test('points(7, 5) ➞ 29', function () {
        const expected = 29
        const twoP = 7
        const threeP = 5
        expect( points(twoP, threeP) ).toBe( expected )
    })
    test('points(38, 8) ➞ 100', function () {
        const expected = 100
        const twoP = 38
        const threeP = 8
        expect( points(twoP, threeP) ).toBe( expected )
    })
})
