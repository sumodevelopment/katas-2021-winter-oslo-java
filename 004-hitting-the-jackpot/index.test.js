import { testJackpot } from './index'

describe('testJackpot', () => {
    test('["@", "@", "@", "@"] -> true', () => {
        const source = ["@", "@", "@", "@"]
        expect( testJackpot( source ) ).toBe( true )
    })

    test('["abc", "abc", "abc", "abc"] -> true', () => {
        const source = ["abc", "abc", "abc", "abc"]
        expect( testJackpot( source ) ).toBe( true )
    })

    test('["SS", "SS", "SS", "SS"] -> true', () => {
        const source =["SS", "SS", "SS", "SS"]
        expect( testJackpot( source ) ).toBe( true )
    })

    test('["&&", "&", "&&&", "&&&&"] -> false', () => {
        const source = ["&&", "&", "&&&", "&&&&"]
        expect( testJackpot( source ) ).toBe( false )
    })

    test('["SS", "SS", "SS", "Ss"] -> false', () => {
        const source =["SS", "SS", "SS", "Ss"]
        expect( testJackpot( source ) ).toBe( false )
    })

})

