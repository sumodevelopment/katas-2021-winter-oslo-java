export const testJackpot = (source = []) => new Set(source).size === 1
