export const VOWEL_LEGEND = {
    a: 1,
    e: 2,
    i: 3,
    o: 4,
    u: 5
}

export function toLowerCaseArray(word = '') {
    return word.toLowerCase().split('')
}

export function replaceVowel(word = '') {
    return toLowerCaseArray(word).map(character => {
        if (VOWEL_LEGEND[character]) {
            return VOWEL_LEGEND[character]
        }
        return character
    }).join('')
}

// short but unreadable. 😢
export const replaceVowelShorter = word => [...word.toLowerCase()]
    .map(c => VOWEL_LEGEND[c] ? VOWEL_LEGEND[c] : c).join('');
