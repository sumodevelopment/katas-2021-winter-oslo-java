import { VOWEL_LEGEND, toLowerCaseArray, replaceVowel, replaceVowelShorter } from './index'

describe('toLowerCaseArray', () => {
    test("heLLo -> ['h', 'e', 'l', 'l', 'o']", () => {
        expect(toLowerCaseArray('heLLo'))
            .toEqual([ 'h', 'e', 'l', 'l', 'o' ])
    })

    test("karAchi -> ['k', 'a', 'r', 'a', 'c', 'h', 'i']", () => {
        expect(toLowerCaseArray('karAchi'))
            .toEqual([ 'k', 'a', 'r', 'a', 'c', 'h', 'i' ])
    })
})

describe('replaceVowel', () => {
    test('karAchi -> k1r1ch3', () => {
        expect(replaceVowelShorter("karAchi")).toBe("k1r1ch3")
    })

    test('chEmBur -> ch2mb5r', () => {
        expect(replaceVowel("chEmBur")).toBe("ch2mb5r")
    })


    test('khandbari -> kh1ndb1r3', () => {
        expect(replaceVowel("khandbari")).toBe("kh1ndb1r3")
    })


    test('LexiCAl -> l2x3c1l', () => {
        expect(replaceVowel("LexiCAl")).toBe("l2x3c1l")
    })


    test('fuNctionS -> f5nct34ns', () => {
        expect(replaceVowel("fuNctionS")).toBe("f5nct34ns")
    })


    test('EASY -> 21sy', () => {
        expect(replaceVowel("EASY")).toBe("21sy")
    })
})

