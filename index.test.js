test('1 + 2 = 3', function () {
    expect(1 + 2).toBe(3)
});

test('hello world!', function () {

    const source = 'hello world!'

    expect(source).toBe('hello world!')
});
