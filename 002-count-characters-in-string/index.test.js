import { countCharacters } from "./index";

test('a in edabit => 1', function (){
    expect(
        countCharacters('a', 'edabit')
    ).toBe(1)
})

test('c in Chamber of secrets => 1', function (){
    expect(
        countCharacters('c', 'Chamber of secrets')
    ).toBe(1)
})

test('b in big fat bubble => 4', function (){
    expect(
        countCharacters('b', 'big fat bubble')
    ).toBe(4)
})
