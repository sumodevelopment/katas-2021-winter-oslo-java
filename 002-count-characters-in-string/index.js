export function countCharacters(character, source) {
    const regExp = new RegExp(character, 'g')
    return (source.match( regExp ) || []).length
}
