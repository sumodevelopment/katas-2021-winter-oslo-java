import { isBoomerang, numberOfBoomerangs } from './'

describe('is boomerang', ()=> {
    test('[3, 7, 3] is boomerang to be true', ()=>{
        const source =  [3, 7, 3]
        expect( isBoomerang( source ) ).toBe( true )
    })

    test('[3, 3, 3] is boomerang to be false', ()=>{
        const source =  [3, 3, 3]
        expect( isBoomerang( source ) ).toBe( false )
    })

    test('[3, -4, 3] is boomerang to be true', ()=>{
        const source =  [3, -4, 3]
        expect( isBoomerang( source ) ).toBe( true )
    })

    test('[9, 2] is boomerang to be false', ()=>{
        const source =  [9,2]
        expect( isBoomerang( source ) ).toBe( false )
    })
})



describe('number of boomerangs', ()=> {
    test('[3, 7, 3, 2, 1, 5, 1, 2, 2, -2, 2] => 3', ()=> {
        const source =  [3, 7, 3, 2, 1, 5, 1, 2, 2, -2, 2]
        expect( numberOfBoomerangs( source ) ).toBe( 3 )
    })

    test('[2, 1, 2, 1, 2, -2, 2, 7] => 2', ()=> {
        const source =  [2, 1, 2, 1, 2, -2, 2, 7]
        expect( numberOfBoomerangs( source ) ).toBe( 2 )
    })

    test('[4, 5, 4] => 1', ()=> {
        const source =  [4, 5, 4]
        expect( numberOfBoomerangs( source ) ).toBe( 1 )
    })

    test('[4, 5] => 0', ()=> {
        const source =  [4, 5]
        expect( numberOfBoomerangs( source ) ).toBe( 0 )
    })

    test('[1, 1, 1, 1, 1, 1, 1] => 0', ()=> {
        const source =  [1, 1, 1, 1, 1, 1, 1]
        expect( numberOfBoomerangs( source ) ).toBe( 0 )
    })
})
