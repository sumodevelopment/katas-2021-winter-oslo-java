import { rearrage } from "."

describe('rearrange', () => {
	test('4of Fo1r pe6ople g3ood th5e the2 -> For the good of the people', () => {
		const source = '4of Fo1r pe6ople g3ood th5e the2'
		const result = 'For the good of the people'
		expect( rearrage( source ) ).toBe( result )
	})

	test('is2 Thi1s T4est 3a -> This is a Test', () => {
		const source = 'is2 Thi1s T4est 3a'
		const result = 'This is a Test'
		expect( rearrage( source ) ).toBe( result )
	})
})


