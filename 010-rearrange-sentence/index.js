export function rearrage(sentence) {
	return sentence.split(' ').reduce((acc, wordWithNum) => {
		const number = wordWithNum.match(/\d/).pop()
		acc[number-1] = wordWithNum.replace(number, '')
		return acc
	}, []).join(' ')
}