export function elasticise(word) {
    const length = word.length
    const half = length / 2
    if (length < 3) return word
    return word.split('').map((char, index) => {
        if (index >= half) { // Right
            return char.repeat(length - index)
        } else { // Left
            return char.repeat(index + 1)
        }
    }).join('')
}
