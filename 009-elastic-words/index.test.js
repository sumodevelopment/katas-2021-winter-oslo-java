import { elasticise } from "./index";

describe('elasticise', () => {

    test('elasticise: ANNA -> ANNNNA', () => {
        expect(elasticise('ANNA')).toBe('ANNNNA')
    })

    test('elasticise: KAYAK -> KAAYYYAAK', () => {
        expect(elasticise('KAYAK')).toBe('KAAYYYAAK')
    })

    test('elasticise: HEK -> HEEK', () => {
        expect(elasticise('HEK')).toBe('HEEK')
    })

    test('elasticise: JACKET -> JAACCCKKKEET', () => {
        expect(elasticise('JACKET')).toBe('JAACCCKKKEET')
    })

    test('elasticise: XY -> XY', () => {
        expect(elasticise('XY')).toBe('XY')
    })

    test('elasticise: X -> X', () => {
        expect(elasticise('X')).toBe('X')
    })

})

